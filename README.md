#Data Types
- most common are Strings and ints
- Each data type can be used differently and behave differently
![DataTypes](./Images/DataTypes.PNG)
- floating point numbers, higher precision used for statistics, money etcc

### Sytactic Sugar
- term used to describe a feature in a language that lets you do something more easily
- makes language "sweeter" for human use
- But doesn't add any new functionality that it didn't already have


# 